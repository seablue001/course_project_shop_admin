import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import '@/utils/facade'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import 'font-awesome/css/font-awesome.min.css'

import 'normalize.css/normalize.css'

Vue.use(ElementUI)

Vue.config.productionTip = false

store.commit('initUserInfo')

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
