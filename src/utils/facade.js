import Vue from 'vue'

import {
  API_SUCCESS_CODE,
  FILE_RESOURCE_PATH
} from '@/config/http'

import {
  APP_MENU_WIDTH
} from '@/config/app'

Vue.prototype.HTTP_CONFIG = {
  API_SUCCESS_CODE
}

Vue.prototype.FILE_RESOURCE_PATH = FILE_RESOURCE_PATH

Vue.prototype.APP_MENU_WIDTH = APP_MENU_WIDTH
