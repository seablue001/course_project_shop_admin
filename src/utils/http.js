import axios from 'axios'
import store from '@/store'
import {
  API_HOST,
  API_VERSION,
  API_TIMEOUT
} from '@/config/http'

const http = axios.create({
  baseURL: API_HOST + API_VERSION,
  timeout: API_TIMEOUT
})

// 请求拦截器
http.interceptors.request.use((config) => {
  config.headers.Authorization = store.state.user.token
  return config
})

// 响应拦截器
http.interceptors.response.use((response) => {
  return response.data
}, (error) => {
  return Promise.reject(error)
})

export default http
