// 数据类型检查
export function isNumber (data) {
  return typeof data === 'number'
}

export function isString () {
  return typeof data === 'string'
}

export function isBoolean () {
  return typeof data === 'boolean'
}

export function isArray (data) {
  return Object.prototype.toString.call(data).slice(8, -1) === 'Array'
}

export function isObject (data) {
  return typeof data === 'object' ? !isArray(data) : false
}

export function isFunction (data) {
  return typeof data === 'function'
}

// 数组元素实现排列组合
export function getCombination (array) {
  // array [['标准版', '定制版'], ['木质'], ['青色', '白色']]
  let resultArray = [] // [['标准版', '木质'], ['定制版', '木质']]
  array.forEach(eleArr => {
    if (resultArray.length === 0) {
      resultArray = eleArr
    } else {
      const tempArr = []
      resultArray.forEach((resEle) => {
        eleArr.forEach((ele) => {
          isArray(resEle) ? tempArr.push([...resEle, ele]) : tempArr.push([resEle, ele])
        })
      })
      resultArray = tempArr
    }
  })
  return resultArray
}

/**
 * [indexOfObjInObjArr 根据指定key, 获取对象obj在对象数组objArr所处位置的索引]
 * @param  {[type]} obj    [description]
 * @param  {[type]} objArr [description]
 * @param  {String} key    [description]
 * @return {[type]}        [description]
 */
export function indexOfObjInObjArrByKey (obj, objArr, key = 'id') {
  let index = -1
  for (let i = 0; i < objArr.length; i++) {
    if (obj[key] === objArr[i][key]) {
      index = i
      break
    }
  }
  return index
}
