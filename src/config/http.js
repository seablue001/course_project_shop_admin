export const API_HOST = process.env.VUE_APP_API_HOST

export const API_VERSION = 'admin/'

export const FILE_RESOURCE_PATH = `${API_HOST}storage/`

export const API_TIMEOUT = process.env.VUE_APP_API_TIMEOUT

export const API_SUCCESS_CODE = 1
