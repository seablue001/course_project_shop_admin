import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import { whiteList } from '@/config/router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('@/components/layout/Index'),
    children: [
      {
        path: '/dashboard',
        component: () => import('@/views/Dashboard'),
        meta: {
          icon: 'fa fa-tachometer',
          title: '首页'
        }
      },
      {
        path: '/base',
        component: () => import('@/components/layout/BridgeRouterView'),
        meta: {
          icon: 'fa fa-cube',
          title: '基础数据'
        },
        children: [
          {
            path: '/specs',
            component: () => import('@/components/layout/BridgeRouterView'),
            meta: {
              title: '规格管理'
            },
            children: [
              {
                path: '/specs-list',
                component: () => import('@/views/specs/list/Index'),
                meta: {
                  title: '规格列表'
                }
              },
              {
                path: '/add-spec',
                component: () => import('@/views/specs/add/Index'),
                meta: {
                  title: '添加规格'
                }
              }
            ]
          },
          {
            path: '/category',
            component: () => import('@/components/layout/BridgeRouterView'),
            meta: {
              title: '分类管理'
            },
            children: [
              {
                path: '/category-list',
                component: () => import('@/views/category/list/Index'),
                meta: {
                  title: '分类列表'
                }
              },
              {
                path: '/add-category',
                component: () => import('@/views/category/add/Index'),
                meta: {
                  title: '添加分类'
                }
              }
            ]
          },
          {
            path: '/brand',
            component: () => import('@/components/layout/BridgeRouterView'),
            meta: {
              title: '品牌管理'
            },
            children: [
              {
                path: '/brand-list',
                component: () => import('@/views/brand/list/Index'),
                meta: {
                  title: '品牌列表'
                }
              },
              {
                path: '/add-brand',
                component: () => import('@/views/brand/add/Index'),
                meta: {
                  title: '添加品牌'
                }
              }
            ]
          },
          {
            path: '/goods',
            component: () => import('@/components/layout/BridgeRouterView'),
            meta: {
              title: '商品管理'
            },
            children: [
              {
                path: '/goods-list',
                component: () => import('@/views/goods/list/Index'),
                meta: {
                  title: '商品列表'
                }
              },
              {
                path: '/add-goods',
                component: () => import('@/views/goods/add/Index'),
                meta: {
                  title: '添加商品'
                }
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/Login')
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  // 登录鉴权
  const token = store.state.user.token
  if (!token && !whiteList.includes(to.path)) {
    next('/login')
  }
  next()
})

export default router
