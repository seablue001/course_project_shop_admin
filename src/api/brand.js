import http from '@/utils/http'

// 品牌列表
export const getBrandListApi = (params) => {
  const url = 'brand-list'
  return http.get(url, { params })
}

/**
 * [brandBaseInfoList 商品品牌基本信息列表]
 * @return {[type]} [description]
 */
export async function brandBaseInfoListApi () {
  const url = 'brand-base-info-list'
  return http.get(url)
}

/**
 * 添加品牌
 * @param {*} params
 * @returns
 */
export const addBrandApi = (params) => {
  const url = 'add-brand'
  return http.post(url, params)
}

/**
 * 更新品牌
 * @param {*} params
 * @returns
 */
export const editBrandApi = (params) => {
  const url = 'edit-brand'
  return http.post(url, params)
}

/**
 * 品牌详情
 * @param {*} params
 * @returns
 */
export const getBrandDetailApi = (params) => {
  const url = 'brand-detail'
  return http.get(url, { params })
}
