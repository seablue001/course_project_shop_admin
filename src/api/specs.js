import http from '@/utils/http'

// 规格列表
export const getSpecsList = () => {
  const url = 'spec-list'
  return http.get(url)
}

/**
 * 添加规格
 * @param {*} params
 * @returns
 */
export const addSpecApi = (params) => {
  const url = 'add-spec'
  return http.post(url, params)
}

/**
 * 添加规格元素
 * @param {*} params
 * @returns
 */
export const addSpecElementsApi = (params) => {
  const url = 'add-spec-element'
  return http.post(url, params)
}

// 规格与元素列表
export const specAndEleBaseInfoListApi = () => {
  const url = 'specs-and-elements-list'
  return http.get(url)
}
