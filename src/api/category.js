import http from '@/utils/http'

/**
 * 分类列表
 * @returns
 */
export const getCategoryListApi = () => {
  const url = 'category-list'
  return http.get(url)
}

/**
 * [categoryBaseInfoList 商品分类基本信息列表]
 * @return {[type]} [description]
 */
export async function categoryBaseInfoListApi () {
  const url = 'category-base-info-list'
  return http.get(url)
}

/**
 * 添加分类
 * @param {*} params
 * @returns
 */
export const addCategoryApi = (params) => {
  const url = 'add-category'
  return http.post(url, params)
}

/**
 * 获取分类基本信息列表
 * @param {*} params
 * @returns
 */
export const getCategoryBaseInfoListApi = () => {
  const url = 'category-base-info-list'
  return http.get(url)
}

/**
 * 获取分类详情信息
 * @param {*} params
 * @returns
 */
export const getCategoryDetailApi = (params) => {
  const url = 'category-detail'
  return http.get(url, { params })
}

/**
 * 编辑分类
 * @param {*} params
 * @returns
 */
export const editCategoryApi = (params) => {
  const url = 'edit-category'
  return http.post(url, params)
}
