import http from '@/utils/http'

// 获取验证码
export const getCaptchaImgSrc = () => {
  const url = 'verify-img'
  return http.get(url)
}

export const loginIn = (params) => {
  const url = 'login-in'
  return http.post(url, params)
}
