import http from '@/utils/http'

/**
 * 商品分页列表
 * @param {*} params
 * @returns
 */
export const getGoodsListApi = (params) => {
  const url = 'goods-list'
  return http.get(url, { params })
}

/**
 * [addGoodsApi 添加商品]
 * @param {[type]} params [description]
 */
export async function addGoodsApi (params) {
  const url = 'add-goods'
  return http.post(url, params)
}
