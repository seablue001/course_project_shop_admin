import { mapState, mapMutations } from 'vuex'

const app = {
  computed: {
    ...mapState({
      isMenuCollapse: state => state.app.isMenuCollapse
    })
  },
  methods: {
    ...mapMutations({
      setIsMenuCollapse: 'setIsMenuCollapse'
    })
  }
}

export default app
