import { mapState, mapMutations } from 'vuex'

const user = {
  computed: {
    ...mapState({
      token: state => state.user.token
    })
  },
  methods: {
    ...mapMutations({
      setUserInfo: 'setUserInfo'
    })
  }
}

export default user
