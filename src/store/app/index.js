import state from './state'
import mutations from './mutations'

const app = {
  state,
  mutations
}

export default app
