const mutations = {
  setIsMenuCollapse (state, status) {
    state.isMenuCollapse = status
  }
}

export default mutations
