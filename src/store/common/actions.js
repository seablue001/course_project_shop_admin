import store from '@/store'
import { API_SUCCESS_CODE } from '@/config/http'
import { getCategoryBaseInfoListApi } from '@/api/category'
const actions = {
  async getCategoryList (context) {
    if (store.state.common.categoryList.length > 0) {
      return false
    }
    const { status, data, message } = await getCategoryBaseInfoListApi()
    if (status === API_SUCCESS_CODE) {
      context.commit('setCategoryList', data || [])
    } else {
      this.$message({
        type: 'error',
        message
      })
    }
  }
}

export default actions
