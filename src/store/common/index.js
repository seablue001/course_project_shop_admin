import state from './state'
import mutations from './mutations'
import actions from './actions'

const user = {
  state,
  mutations,
  actions
}

export default user
