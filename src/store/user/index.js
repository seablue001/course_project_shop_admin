import state from './state'
import mutations from './mutations'

const user = {
  state,
  mutations
}

export default user
