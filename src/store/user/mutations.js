const mutations = {
  setUserInfo (state, user) {
    for (const attr in user) {
      state[attr] = user[attr]
    }

    // 将数据持久化处理
    localStorage.setItem('userInfo', JSON.stringify(user))
  },
  initUserInfo (state) {
    const cacheUser = localStorage.getItem('userInfo')
    if (cacheUser === 'undefined') {
      return false
    }
    const user = JSON.parse(cacheUser)
    mutations.setUserInfo(state, user)
  }
}

export default mutations
