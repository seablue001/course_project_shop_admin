export const categoryFormRules = (() => {
  const validateCategoryName = (rule, value, callback) => {
    if (!value || value.trim() === '') {
      callback(new Error('请输入分类名称'))
    } else {
      callback()
    }
  }

  const validateLogo = (rule, value, callback) => {
    if (value.length === 0) {
      callback(new Error('请选择logo'))
    } else {
      callback()
    }
  }

  const validateParentCategory = (rule, value, callback) => {
    if (!value) {
      callback(new Error('请选择上级分类'))
    } else {
      callback()
    }
  }

  return {
    categoryName: [
      { required: true, validator: validateCategoryName, trigger: 'blur' }
    ],
    parentCategory: [
      { required: true, validator: validateParentCategory, trigger: 'blur' }
    ],
    logo: [
      { required: true, validator: validateLogo, trigger: 'blur' }
    ]
  }
})()
