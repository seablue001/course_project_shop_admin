/* 商品 Validator */
import {
  isArray
} from '@/utils'

import {
  isPositiveIntegerReg,
  isPositive,
  isRealNumber
} from '@/utils/reg'

const goodsRules = (() => {
  const validatorName = (rule, value, callback) => {
    if (value === '' || !value) {
      callback(new Error('请输入商品名称'))
    }
    callback()
  }

  const validatorBrandId = (rule, value, callback) => {
    if (value === '' || !value) {
      callback(new Error('请选择商品所属品牌'))
    }
    callback()
  }

  const validatorCategoryId = (rule, value, callback) => {
    if (value === '') {
      callback(new Error('请选择商品所属分类'))
    }
    callback()
  }

  const validatorContent = (rule, value, callback) => {
    if (value === '' || !value) {
      callback(new Error('请输入商品内容描述'))
    }
    callback()
  }

  const validatorUnit = (rule, value, callback) => {
    if (value === '' || !value) {
      callback(new Error('请输入商品计量单位'))
    }
    callback()
  }

  const validatorSort = (rule, value, callback) => {
    if (value !== '') {
      if (!isRealNumber.test(value)) {
        callback(new Error('商品排序必须为数字'))
      }
    }
    callback()
  }

  const validatorSkuList = (rule, value, callback) => {
    if (!isArray(value) || value.length <= 0) {
      callback(new Error('请完善商品sku规格库存等信息'))
    }
    callback()
  }

  const validatorSkuName = (rule, value, callback) => {
    if (value === '' || !value) {
      callback(new Error('请输入商品sku规格昵称'))
    }
    callback()
  }

  const validatorSkuStock = (rule, value, callback) => {
    if (value === '') {
      callback(new Error('请输入商品sku库存'))
    } else {
      if (!isPositiveIntegerReg.test(value)) {
        callback(new Error('商品sku库存必须为正整数'))
      }
    }
    callback()
  }

  const validatorSkuPrice = (rule, value, callback) => {
    if (value === '') {
      callback(new Error('请输入商品sku售价'))
    } else {
      if (!isPositive.test(value)) {
        callback(new Error('商品sku售价必须为0或正实数'))
      }
    }
    callback()
  }

  const validatorSkuMarketPrice = (rule, value, callback) => {
    if (value === '' || !value) {
      callback(new Error('请输入商品sku市场价'))
    } else {
      if (!isPositive.test(value)) {
        callback(new Error('商品sku市场价必须为0或正实数'))
      }
    }
    callback()
  }

  const validatorSkuWeight = (rule, value, callback) => {
    if (value === '' || !value) {
      callback(new Error('请输入商品sku重量'))
    } else {
      if (!isPositive.test(value)) {
        callback(new Error('商品sku重量必须为0或正实数'))
      }
    }
    callback()
  }

  const validatorSkuSort = (rule, value, callback) => {
    if (value !== '') {
      if (!isRealNumber.test(value)) {
        callback(new Error('商品sku排序必须为数字'))
      }
    }
    callback()
  }

  const validatorSkuImageFiles = (rule, value, callback) => {
    if (!isArray(value) || value.length <= 0) {
      callback(new Error('请上传商品sku展示图片'))
    }
    callback()
  }

  return {
    goodsName: [
      { required: true, validator: validatorName, trigger: 'blur' }
    ],
    brandId: [
      { required: true, validator: validatorBrandId, trigger: 'blur' }
    ],
    categoryId: [
      { required: true, validator: validatorCategoryId, trigger: 'blur' }
    ],
    content: [
      { required: true, validator: validatorContent, trigger: 'blur' }
    ],
    unit: [
      { required: true, validator: validatorUnit, trigger: 'blur' }
    ],
    sort: [
      { validator: validatorSort, trigger: 'blur' }
    ],
    skuList: [
      { required: true, validator: validatorSkuList, trigger: 'blur' }
    ],
    skuName: [
      { required: true, validator: validatorSkuName, trigger: 'blur' }
    ],
    skuStock: [
      { required: true, validator: validatorSkuStock, trigger: 'blur' }
    ],
    skuPrice: [
      { required: true, validator: validatorSkuPrice, trigger: 'blur' }
    ],
    skuMarketPrice: [
      { required: true, validator: validatorSkuMarketPrice, trigger: 'blur' }
    ],
    skuWeight: [
      { required: true, validator: validatorSkuWeight, trigger: 'blur' }
    ],
    skuSort: [
      { validator: validatorSkuSort, trigger: 'blur' }
    ],
    skuImageFiles: [
      { required: true, validator: validatorSkuImageFiles, trigger: 'blur' }
    ]
  }
})()

export default goodsRules
