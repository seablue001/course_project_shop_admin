export const brandFormRules = (() => {
  const validateBrandName = (rule, value, callback) => {
    if (!value || value.trim() === '') {
      callback(new Error('请输入商品品牌名称'))
    } else {
      callback()
    }
  }

  return {
    brandName: [
      { required: true, validator: validateBrandName, trigger: 'blur' }
    ]
  }
})()
