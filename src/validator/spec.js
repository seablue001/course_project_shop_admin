export const specEleFormRules = (() => {
  const validateSpecElesList = (rule, value, callback) => {
    if (!value || value.trim() === '') {
      callback(new Error('请输入商品规格元素名称'))
    } else {
      callback()
    }
  }

  return {
    specName: [
      { required: true, message: '规格名称不能为空', trigger: 'blur' }
    ],
    specElesList: [
      { required: true, validator: validateSpecElesList, trigger: 'blur' }
    ]
  }
})()
